document.getElementById('hello_text').textContent = "はじめてのJavaScript";

var count = 0;
var maxcount = 0;
var genecount = 0;
var cells;    //ゲーム盤を示す変数

// ブロックのパターン
var blocks = {
  i: {          //i型ブロック自身を示す。　blocks["i"]で呼び出せる。
    class: "i",             //blocks["i"].classで呼び出せる。　その値は"i"。
    pattern: [[1, 1, 1, 1]] //blocks["i"].patternで呼び出せる。
  },
  o: {
    class: "o",
    pattern: [
      [1, 1],
      [1, 1]
    ]
  },
  t: {
    class: "t",
    pattern: [
      [0, 1, 0],
      [1, 1, 1]
    ]
  },
  s: {
    class: "s",
    pattern: [
      [0, 1, 1],
      [1, 1, 0]
    ]
  },
  z: {
    class: "z",
    pattern: [
      [1, 1, 0],
      [0, 1, 1]
    ]
  },
  j: {
    class: "j",
    pattern: [
      [1, 0, 0],
      [1, 1, 1]
    ]
  },
  l: {
    class: "l",
    pattern: [
      [0, 0, 1],
      [1, 1, 1]
    ]
  }
};//*/ブロックのパターン


loadTable();  //ゲーム盤を読み込む

// setInterval(動かしたい関数, 繰り返す間隔(ms));
setInterval(function () {
  // 何回目かを数えるために変数countを1ずつ増やす
  count++;
  // 何回目かを文字にまとめて表示する
  document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")\nブロック数最高記録:" + maxcount;
  /*"hello_text"ID属性をもつHTML要素をgetElementById()メソッドで取得し、textContentに代入*/



  // ブロックが積み上がり切っていないかのチェック
  // setTimeout(() => {
  //   for (var row = 0; row < 2; row++) {      //2列目までに
  //     for (var col = 0; col < 10; col++) {
  //       if (cells[row][col].className !== "") { //ブロックがあったら
  //         alert("game over");
  //       }
  //     }
  //   }
  // }, 2);

  if (hasFallingBlock()) {   // 落下中のブロックがあるか確認する
    fallBlocks();           // あればブロックを落とす
  } else {                   // 落下中のブロックがなければ
    deleteRow();            // そろっている行を消す
    setTimeout(gameOverCheck(), 2);
    generateBlock();        // ランダムにブロックを作成する
  }
}, 1000);//*/setInterval()

var flag = false;

//ブロックが積み上がっていないかのチェック
function gameOverCheck() {
  for (var row = 0; row < 2; row++) {      //2列目までに
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].className !== "") { //ブロックがあったら

        flag = true;
      }
    }
  }
  if (flag) {
    alert("GameOver");
    flag = false;
    updateCount();
    loadTable();
  }
}

//ゲーム盤の状態を2次元配列にまとめる
function loadTable() {
  /* td_arrayには全部で200マス分の<td>タグが格納
    左上から0, 1, ..., 199*/
  cells = [];
  var td_array = document.getElementsByTagName("td");
  var index = 0;
  for (var row = 0; row < 20; row++) {
    cells[row] = [];
    // 配列のそれぞれの要素を配列にする(2次元配列にする)
    for (var col = 0; col < 10; col++) {
      cells[row][col] = td_array[index];
      index++;
      cells[row][col].className = "";
    }
  }
}

//ブロックを一つ下に移動させる
function fallBlocks() {
  /*
     // 一番下の行にブロックがあれば落下中のフラグをfalseにする
    for (var i = 0; i < 10; i++) {
      if(cells[19][i].className !== ""){  //一番下のクラスがnullでないなら(一番下にブロックがあるなら)
        isFalling = false;  //落下中でない
        return;             //落とさない
      }
    }
    //落下中なら
    // 下から二番目の行から繰り返しクラスを下げていく
    for (var row = 18; row >= 0; row--) {
      for (var col = 0; col < 10; col++) {
        if (cells[row][col].className !== "") {//そのcellのクラス名がnullでなかったら
          cells[row + 1][col].className = cells[row][col].className;//一個下にそのクラス名をつける
          cells[row][col].className = "";//上のクラス名はnullにする
        }
      }
    }
  */

  // 1. 底についていないか？
  for (var col = 0; col < 10; col++) {
    if (cells[19][col].blockNum === fallingBlockNum) { //一番下のブロック番号が落下中のブロック番号に一致していたら
      isFalling = false;        //落下停止
      return;                   // 一番下の行にブロックがいるので落とさない
    }
  }
  // 2. 1マス下に別のブロックがないか？
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {//注目しているcellが落下中なら
        if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
          //そのcellの一個下にブロックが無い　かつ　一個下のブロック番号が落下中のブロック番号に一致していないなら
          isFalling = false;
          return;               // 一つ下のマスにブロックがいるので落とさない
        }
      }
    }
  }

  //1にも2にも該当しないとき
  // 下から二番目の行から繰り返しクラスを下げていく
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}//*/fallBlocks()

/*
1 一番下の行を見る
2 クラスを空にする
3 下から2番目の行を見る
4 もしもクラスが振られていたら一行下のマスにクラスを写して自分のクラスを空にする
5 4を一番上の行に達するまで繰り返す
*/

var isFalling = false;
function hasFallingBlock() {
  // 落下中のブロックがあるか確認する
  return isFalling;;
}

// そろっている行を消す
function deleteRow() {
  for (var row = 19; row >= 0; row--) {    //ある行において
    var canDelete = true;
    for (var col = 0; col < 10; col++) {   //端から順にチェック
      if (cells[row][col].className === "") { //一つでもclassNameが付いていないものがあれば
        canDelete = false;                    //消さない
      }
    }

    if (canDelete) {
      //1行消す
      for (var col = 0; col < 10; col++) {
        cells[row][col].className = "";
      }
      // 上の行のブロックをすべて1マス落とす
      for (var downRow = row - 1; row >= 0; row--) {
        for (var col = 0; col < 10; col++) {
          cells[downRow + 1][col].className = cells[downRow][col].className;
          cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
          cells[downRow][col].className = "";
          cells[downRow][col].blockNum = null;
        }
      }
    }//*/canDelete
  }
}//*/deleteRow()

var fallingBlockNum = 0;    //生成するブロックに番号を振る
// ランダムにブロックを生成する
function generateBlock() {
  genecount++;

  // 1. ブロックパターンからランダムに一つパターンを選ぶ
  var keys = Object.keys(blocks); //名前の配列を取得
  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];//名前の一覧から要素をランダムに選ぶ
  //keys.lengthは配列数
  //Math.floorで小数点以下を切り捨て
  var nextBlock = blocks[nextBlockKey];
  var nextFallingBlockNum = fallingBlockNum + 1;

  // 2. 選んだパターンをもとにブロックを配置する
  var pattern = nextBlock.pattern;
  for (var row = 0; row < pattern.length; row++) {
    for (var col = 0; col < pattern[row].length; col++) {
      if (pattern[row][col]) {      //[0,3]から生成
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
      }
    }
  }

  // 3. 落下中のブロックがあるとする  1秒経った時にまたブロックを生成し続けるのを防ぐ
  isFalling = true;
  fallingBlockNum = nextFallingBlockNum;

}//*/generateBlock()


// キーボードイベントを監視する
//対象要素.addEventListener( 種類, 関数, (通常は)false )
document.addEventListener("keydown", onKeyDown);


// キー入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
  if (event.keyCode === 37) {
    //moveLeft();
    Check_and_moveLeft();
  } else if (event.keyCode === 39) {
    //moveRight();
    Check_and_moveRight();
  }
}

// ブロックを右に移動させる
/*
function moveRight() {
  for (var row = 0; row < 20; row++) { //&lt;は<と同じ
    for (var col = 9; col >= 0; col--) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}
*/
// ブロックを左に移動させる
/*
function moveLeft() {
  // ブロックを左に移動させる
  for (var row = 0; row < 20; row++) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col - 1].className = cells[row][col].className;
        cells[row][col - 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}
*/
function Check_and_moveRight() {
  //1. 右端についていないか？
  for (var row = 0; row < 19; row++) {
    if (cells[row][9].blockNum === fallingBlockNum) {
      //isFalling = true;        //落下停止,はしない
      return;                   // 何もしない
    }
  }
  // 2. 1マス右に別のブロックがないか？
  for (var row = 19; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {//注目しているcellが落下中なら
        if (cells[row][col + 1].className !== "" && cells[row][col + 1].blockNum !== fallingBlockNum) {
          //isFalling = true;
          return;               //何もしない
        }
      }
    }
  }

  for (var row = 0; row < 20; row++) {
    for (var col = 9; col >= 0; col--) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}


function Check_and_moveLeft() {
  // 1. 左端についていないか？
  for (var row = 0; row < 20; row++) {
    if (cells[row][0].blockNum === fallingBlockNum) {
      //isFalling = false;        //落下停止,はしない
      return;                   // 何もしない
    }
  }
  // 2. 1マス右に別のブロックがないか？
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {//注目しているcellが落下中なら
        if (cells[row][col - 1].className !== "" && cells[row][col - 1].blockNum !== fallingBlockNum) {
          return;               //何もしない
        }
      }
    }
  }

  // ブロックを左に移動させる
  for (var row = 0; row < 20; row++) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col - 1].className = cells[row][col].className;
        cells[row][col - 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

function updateCount() {
  if (maxcount < genecount) {
    maxcount = genecount;
  }
  genecount = 0;
}
